# GitlabStatus

This gem is the response to the following exercise:
Write a small Ruby gem that exposes a CLI to check the status of https://gitlab.com or https://about.gitlab.com and reports an average response time after probing the site every 10 seconds for a one minute. There is no need to publish the gem on rubygems.org. Please make this project as complete as you think it should be to be maintainable in a long term by more than one maintainer


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab_status'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gitlab_status

## Usage
  Commands:

  gitlab_status 'home'

  gitlab_status 'about'

  if no argument is given gitlab_status will choose home by default.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/lmhsjackson/gitlab_status. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GitlabStatus project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/gitlab_status/blob/master/CODE_OF_CONDUCT.md).
